<?php if (!empty($errors)): ?>
    <?php foreach ($errors as $e): ?>
        <div class="warning"><?php echo $e; ?></div>
    <?php endforeach;?>
<?php else: ?>
    <?php if ($payplug_popup == 0): ?>
    <p align="right">
        <b>
            <?php echo $text_bank_redirect; ?>
        </b>
    </p>
    <div class="buttons">
        <div class="right">
            <form action="">
                <input type="button" value="<?php echo $button_confirm; ?>" class="button" onclick="location.href = '<?php echo $paymentUrl; ?>';"/>
            </form>
        </div>
    </div>
    <?php else: ?>
    <script type="text/javascript" src="http://www.payplug.fr/static/button/scripts/payplug.js"></script>
    <div class="buttons">
        <div class="right">
            <form action="">
                <input type="button" value="<?php echo $button_confirm; ?>" class="button" onclick="Payplug.showPayment('<?php echo $paymentUrl; ?>');"/>
            </form>
        </div>
    </div>
    <?php endif; ?>
<?php endif; ?>