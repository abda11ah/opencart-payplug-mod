### Module de paiement PayPlug pour Opencart ###

* Version : 1.1
* Licence : Creative Commons (CC BY-NC-SA 4.0) (http://creativecommons.org/licenses/by-nc-sa/4.0/)

### Page de téléchargement ###

[http://opencart-france.fr/index.php?route=extension/extension/info&extension_id=109](http://opencart-france.fr/index.php?route=extension/extension/info&extension_id=109)

### Installation ###

Avant l'installation, assurez-vous d'avoir au préalable ouvert un compte chez PayPlug (opération gratuite et instantanée)

* Copiez tous les fichiers dans le répertoire racine de Opencart.
* Connectez-vous à http://www.votresiteweb.com/admin
* Vérifiez tout d'abord que Opencart soit configuré pour utiliser la devise EURO.
(Attention le module Payplug ne fonctionne pas avec les autres devises !)
* Entrez dans le menu [Extensions] puis [Paiements]
* Sélectionnez PayPlug et cliquez sur [Installer]
* Sélectionnez à nouveau PayPlug et cliquez sur [Modifier]
* Renseignez les champs [Courriel] et [Mot de passe] et faites correspondre les statuts [commande payée] et [commande annulée] par [commande traitée] et [commande remboursée]
* Sélectionnez [Etat] puis [Activé]

Dans le cas où PayPlug évoluerait et supporterait une autre devise que l'euro, ajoutez la devise nécessaire dans le fichier [/catalog/model/payment/payplug.php] (dans le tableau $currencies).

Si lorsque vous arrivez sur la page de paiement vous voyez apparaître le message :
"Aucune transaction ne correspond à votre requête."
Eh bien cela signifie que votre compte Payplug n'est pas encore activé à 100%. Contactez Payplug pour résoudre le problème.

Notez que le module ne fonctionnera pas correctement en mode local (http://localhost ou http://127.0.0.1) parce qu'il ne pourra pas recevoir de confirmation de paiement ou remboursement depuis le serveur Payplug.

### À propos de PayPlug ###

PayPlug est une solution de paiement par carte bancaire qui fonctionne sans contrat de vente à distance (VAD).

Le client n'a pas besoin de compte pour payer et la page de paiement personnalisable a été optimisée pour la conversion.

La solution ne comporte ni abonnement, ni engagement. Vous ne payez que s'il y a des ventes. Les tarifs sont dégressifs selon votre volume de paiement mensuel ( de 2,5% à 0,8% + 0,25€ par transaction). 

PayPlug transfère ensuite les fonds vers votre banque par virement européen (SEPA).

Les pays supportés par le système sont, pour l'instant, tous les pays supportant la devise EURO et les virements bancaires de type SEPA.

Si tout fonctionne comme sur des roulettes, je vous invite à faire un don pour aider au développement de modules meilleurs :
[http://wherbmaster.tumblr.com/post/113999137976/module-de-paiement-payplug-pour-opencart](http://wherbmaster.tumblr.com/post/113999137976/module-de-paiement-payplug-pour-opencart)

----------------------------------------------------------------