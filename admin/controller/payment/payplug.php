<?php

class ControllerPaymentPayplug extends Controller {

    private $errors = array();
    private $PayPlug;

    public function index() {

        $this->load->language('payment/payplug');
        require DIR_SYSTEM.'payplug.inc.php';
        $this->PayPlug = new PayPlug();

        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            if ($this->validate()) {
                $this->model_setting_setting->editSetting('payplug', $this->request->post);
                $this->session->data['success'] = $this->language->get('text_success');
                $this->redirect($this->url->link('extension/payment', 'token='.$this->session->data['token'], 'SSL'));
            }
        }

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_all_zones'] = $this->language->get('text_all_zones');
        $this->data['text_module_disabled'] = $this->language->get('text_module_disabled');
        $this->data['entry_email'] = $this->language->get('entry_email');
        $this->data['entry_password'] = $this->language->get('entry_password');
        $this->data['entry_refunded_status'] = $this->language->get('entry_refunded_status');
        $this->data['entry_paid_status'] = $this->language->get('entry_paid_status');
        $this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $this->data['entry_popup'] = $this->language->get('entry_popup');
        $this->data['entry_sandbox'] = $this->language->get('entry_sandbox');
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token='.$this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_payment'),
            'href' => $this->url->link('extension/payment', 'token='.$this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('payment/payplug', 'token='.$this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('payment/payplug', 'token='.$this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/payment', 'token='.$this->session->data['token'], 'SSL');

        if (isset($this->request->post['payplug_email'])) {
            $this->data['payplug_email'] = $this->request->post['payplug_email'];
        } else {
            $this->data['payplug_email'] = $this->config->get('payplug_email');
        }

        if (isset($this->request->post['payplug_password'])) {
            $this->data['payplug_password'] = $this->request->post['payplug_password'];
        } else {
            $this->data['payplug_password'] = $this->config->get('payplug_password');
        }

        if (isset($this->request->post['payplug_sandbox'])) {
            $this->data['payplug_sandbox'] = $this->request->post['payplug_sandbox'];
        } else {
            $this->data['payplug_sandbox'] = $this->config->get('payplug_sandbox');
        }

        if (isset($this->request->post['payplug_popup'])) {
            $this->data['payplug_popup'] = $this->request->post['payplug_popup'];
        } else {
            $this->data['payplug_popup'] = $this->config->get('payplug_popup');
        }

        if (isset($this->request->post['payplug_refunded_status_id'])) {
            $this->data['payplug_refunded_status_id'] = $this->request->post['payplug_refunded_status_id'];
        } else {
            $this->data['payplug_refunded_status_id'] = $this->config->get('payplug_refunded_status_id');
        }

        if (isset($this->request->post['payplug_paid_status_id'])) {
            $this->data['payplug_paid_status_id'] = $this->request->post['payplug_paid_status_id'];
        } else {
            $this->data['payplug_paid_status_id'] = $this->config->get('payplug_paid_status_id');
        }

        $this->load->model('localisation/order_status');
        $this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->request->post['payplug_geo_zone_id'])) {
            $this->data['payplug_geo_zone_id'] = $this->request->post['payplug_geo_zone_id'];
        } else {
            $this->data['payplug_geo_zone_id'] = $this->config->get('payplug_geo_zone_id');
        }

        $this->load->model('localisation/geo_zone');

        $this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        if (isset($this->request->post['payplug_status'])) {
            $this->data['payplug_status'] = $this->request->post['payplug_status'];
        } else {
            $this->data['payplug_status'] = $this->config->get('payplug_status');
        }

        if (isset($this->request->post['payplug_sort_order'])) {
            $this->data['payplug_sort_order'] = $this->request->post['payplug_sort_order'];
        } else {
            $this->data['payplug_sort_order'] = $this->config->get('payplug_sort_order');
        }

        if (!$this->PayPlug->init) {
            $this->errors[] = sprintf($this->language->get('error_module_failed'), $this->PayPlug->errModule, $this->PayPlug->errCode, $this->PayPlug->errMsg);
        }

        if (!empty($this->errors)) {
            foreach ($this->errors as $e) {
                $this->data['errors'][] = $e;
            }
        }

        // Disable the module if an error was found
        if (!empty($this->data['errors'])) {
            $this->data['payplug_status'] = 0;
            $this->model_setting_setting->editSetting('payplug', array('payplug_status' => 0));

            $authFile = DIR_CONFIG.'payplug_auth.json';
            if (file_exists($authFile)) {
                unlink($authFile);
            }
        }

        $this->template = 'payment/payplug.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
        $this->data['payplug_init'] = $this->PayPlug->init;
        $this->response->setOutput($this->render());
    }

    private function validate() {

        if (!$this->user->hasPermission('modify', 'payment/payplug')) {
            $this->errors[] = $this->language->get('error_permission');
        }

        if (!$this->request->post['payplug_email']) {
            $this->errors[] = $this->language->get('error_email');
        }

        if (!$this->request->post['payplug_password']) {
            $this->errors[] = $this->language->get('error_password');
        }

        if (empty($this->request->post['payplug_sort_order'])) {
            $this->request->post['payplug_sort_order'] = 0;
        }

        $sandbox = ($this->request->post['payplug_sandbox'] == '1' ? true : false);

        $auth = $this->PayPlug->serverAuth($this->request->post['payplug_email'], $this->request->post['payplug_password'], $sandbox);
        if ($auth !== false) {
            $file = DIR_CONFIG.'payplug_auth.json';
            if (is_writable(DIR_CONFIG)) {
                file_put_contents($file, $auth);
                return true;
            } else {
                $this->errors[] = sprintf($this->language->get('error_writing_auth_file'), $file);
                return false;
            }
        }
        $this->errors[] = sprintf($this->language->get('error_module_failed'), $this->PayPlug->errModule, $this->PayPlug->errCode, $this->PayPlug->errMsg);
        return false;
    }

}
